package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;

    public static void main(String[] args) {
        // Initialiserer de statiske variablene med nye Pokémon-objekter.
        pokemon1 = new Pokemon("Pikachu", 100, 30);
        pokemon2 = new Pokemon("Bulbasaur", 100, 25);

        // Skriver ut den initiale statusen for begge Pokémonene.
        System.out.println(pokemon1);
        System.out.println(pokemon2);

        // La Pokémonene kjempe til en av dem er beseiret.
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
        }


    }
}
